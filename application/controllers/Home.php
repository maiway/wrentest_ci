<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	var $arrData;
	
	function __construct() {
        parent::__construct();
        $this->load->model(array('Csv_model','General_model'));
    }

	public function index($mode='')
	{
		$mode = $mode != 'test' ? 1 : 0;
		# extract data from csv
		$data = $this->Csv_model->extract('files/stock.csv');
		
		/**I used array search and strtolower to find stock, cost in gbp and discontinued from header, so in case of column changes.*/
		# set index of stock
		$stock_index = array_search('stock', array_map('strtolower', $data[0]));
		# set index of cost
		$cost_index  = array_search('cost in gbp', array_map('strtolower', $data[0]));
		# set index of discontinued
		$discont_index  = array_search('discontinued', array_map('strtolower', $data[0]));

		# initialization of total process, success and skip
		$total_process = 0;
		$total_success = 0;
		$total_skip = 0;

		# Loop all rows starting index 1, where index 0 is the header
		foreach(array_slice($data,1) as $line):
			# counting process row
			$total_process = $total_process + 1;
			# get validated and fixed data and set in variable process
			$fixed_data = $this->Csv_model->fix_line($line);
			$process = $this->General_model->validate_line($fixed_data,$stock_index,$cost_index,$discont_index);
			# check if process status is success
			if($process['status'] == 'success'):
				# then count success
				$total_success = $total_success + 1;
				# get process data
				$process_data = $process['data'];
				# check mode; if mode is 'test', insertion of database is not applicable;
				if($mode):
					# insertion of data
					$arrData = array(
						'strProductName' => $process_data[1],
						'strProductDesc' => $process_data[2],
						'strProductCode' => $process_data[0],
						'dtmAdded'   	 => date('Y-m-d'),
						'dtmDiscontinued'=> $process_data[6]);
					$this->General_model->insert($arrData);
				endif;
			else:
				# count skip data
				$total_skip = $total_skip + 1;
			endif;
		endforeach;

		$this->arrData['total_process'] = $total_process;
		$this->arrData['total_success'] = $total_success;
		$this->arrData['total_skip'] = $total_skip;

		$this->load->view('generate',$this->arrData);
	}
}
