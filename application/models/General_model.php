<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class General_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();	
	}

	# this function is for validation; import rules applied
	public function validate_line($data,$stock_i,$cost_i,$discont_i)
	{
		# Any stock item which costs less that £5 and has less than 10 stock will not be imported.
		if(!($data[$cost_i] < 5 && $data[$stock_i] < 10)):
			# Any stock items which cost over £1000 will not be imported.
			if(!($data[$cost_i] > 1000)):
				# Any stock item marked as discontinued will be imported, but will have the discontinued date set as the current date.
				if($data[$discont_i] == strtolower('yes')):
					# imported data will have the discontinued date set as the current date
					$data[6] = date('Y-m-d');
					return array('status' => 'success', 'data' => $data);
				else:
					return array('status' => 'skip', 'data' => $data);
				endif;
			else:
				return array('status' => 'skip', 'data' => $data);
			endif;
		else:
			return array('status' => 'skip', 'data' => $data);
		endif;
	}

	# this function is for insert row in database
	public function insert($arrData)
	{
		$this->db->insert('tblProductData', $arrData);
		return $this->db->insert_id();
	}

}