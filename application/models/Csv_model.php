<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Csv_model extends CI_Model {

	# extraction of csv function
	public function extract($location)
	{
		# opening a file using fopen()
	    $pointer = fopen($location,"r");
	    # initialized data
	    $data = array();

	    # while pointer is not end of file
	    while(! feof($pointer)):
	    	# add csv line/row in array $data
	    	$data[] = fgetcsv($pointer);
	    endwhile;

	    # return the result
	    return $data;
	}

	# this function is for fixing unformatted csv line, some issues like special characters for number columns and others.
	public function fix_line($data)
	{
		# initialization of new_array variable, this is the variable storage of newly fixed data
		$new_array = array('','','','','','');
		# check if data array lenght not 6, it means, this is an unformatted line
		if(count($data) != 6):
			# check if data length is greater than 6
			if(count($data) > 6):
				# new_array[0] and new_array[1] or $data[0] and $data[1] will remain in the index 0 and 1; assuming data in the index 0 and 1 are correct
				$new_array[0] = $data[0];
				$new_array[1] = $data[1];
				# get the remaining element from index[0] to index[1] and 3 indexes from last element; assuming that this elements are supposed to be combined and in index [2]
				$new_array[2] = implode('',array_slice($data,2,count($data)-5));
				# get the third to the last element of data, assuming the third to the last element is supposed to be in index[3]
				$new_array[3] = $data[count($data) - 3];
				# get the second to the last element of data, assuming the second to the last element is supposed to be in index[4]
				$new_array[4] = $data[count($data) - 2];
				# get the last element of data, assuming the last element is suppose to be in index[5]
				$new_array[5] = end($data);
				# set new array in data
				$data = $new_array;
			else:
				# assuming the data[0] is correct
				$new_array[0] = $data[0];

				# check if 1 in data is exists
				if(count($data) > 1):
					if(!is_numeric($data[1])):
						# if data[1] is not numeric set to new_array[1]
						$new_array[1] = $data[1];
					else:
						# else data[1] will set to new_array[3]
						$new_array[3] = $data[1];
					endif;
				endif;

				# check if 2 in data is exists
				if(count($data) > 2):
					if(!is_numeric($data[2])):
						# if data[2] is not numeric and string lenght is less than or equal to 3 [based on data[5] == 'yes'] set to new_array[2]
						if(strlen($data[2]) <= 3):
							$new_array[5] = $data[2];
						else:
							$new_array[2] = $data[2];
						endif;
					else:
						# else data[2] will set to new_array[4]
						$new_array[4] = $data[2];
					endif;
				endif;

				# check if 3 in data is exists
				if(count($data) > 3):
					if(!is_numeric($data[3])):
						# if data[3] is not numeric and string lenght is less than or equal to 3 [based on data[5] == 'yes'] set to new_array[5]
						if(strlen($data[3]) <= 3):
							$new_array[5] = $data[3];
						endif;
					else:
						# else data[3] will set to new_array[3]
						$new_array[3] = $data[3];
					endif;
				endif;

				$data = $new_array;
			endif;
		endif;

		# fix cost and stock data with $ sign
		$data[3] = str_replace('$','',$data[3]);
		$data[4] = str_replace('$','',$data[4]);
		return $data;
	}

}