Framework: Codeigniter

CSV File: files/stock.csv

Database Setup File Location: application/config/database.php

Database Schema Location: files/make_database.sql [no alteration in the schema]

To run in test mode: php index.php home index test
else just run: php index.php home index

```
Why no alteration of schema?
```
The code will validate and fix potential data encoding issues or line termination problems.

```
Either address these concerns in the code or indicate in your response how you would tackle these issues if you had more time to develop your script.
```
Additional conditions for data validation.
